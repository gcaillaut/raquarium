alga <- function(age = 0) {
    obj <- living_being(age)
    class(obj) <- c(class(obj), "alga")
    obj
}


toString.alga <- function(x) {
    paste("An alga with", x$life, "life points")
}


reproduce.alga <- function(x) {
    if (x$life >= 10) {
        new_life <- x$life %/% 2
        x$life <- new_life

        child <- alga()
        child$life <- new_life

        list(x, child)
    }
    else {
        list(x)
    }
}